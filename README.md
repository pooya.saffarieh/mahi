Mahi
====

Author: P. Saffarieh (p.saffarieh@nikhef.nl)

A set of tools that accelarate simulation and analysis of dynamical systems.

## Importing with first building and installation.

CD into the path. Activate correct conda env.

1- Build:
python setup.py bdist_wheel 

2 - Install:
pip install --force-reinstall ./dist/latest_version_file.whl

Note: the latest version doesn't change unless manually done so.

3 - Re-import 


## Importing without installation.

Or you can instead directly import this library into your notebook by:

1- Add the library/module path to the python search paths:


import sys
sys.path.append('path/to/mahi/mahi/')

2- Import the library

from linearsys import zpk2tf

Note: If you already installed mahi you can:
?mahi or ?linearsys
To see which source has been used.

It would be better to directly load module instead of whole library as it might make python confused which should be loaded.

## Install in pip edit mode (development mode)

You can also install the library using edit mode option of pip. Just cd into the mahi directory and run:

pip install -e .

and in your jupyter notebook add this cell;

%load_ext autoreload
%autoreload 2

By changing files in mahi changes apply in the notebook without the need to installation or reloading everything.