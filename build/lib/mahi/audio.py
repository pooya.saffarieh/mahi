'''

Functions which export a signal into audible signal.

Author: P. Saffarieh (p.saffarieh@nikhef.nl)

'''

from scipy.io.wavfile import write

def write_wav(signal,filename,rate,remove_mean=True,bit=32):
    
    '''
    signal: 1d numpy array - input signal to convert 
    filename: file path + name + ".wav" to store the file
    rate:   sample rate of audio. If sample rate not equal to original rate result in freq. shift.
    remove_mean: Center the data. Advised!
    bit: 32 (max=2147483647) or 16 (max=32767). 
    
    For more information see: https://docs.scipy.org/doc/scipy/reference/generated/scipy.io.wavfile.write.html
    '''
    if bit == 32:
        max_   = 2147483647
        signal = np.int32(signal / np.max(np.abs(signal)) * max_)
    elif bit == 16:
        max_   = 32767
        signal = np.int16(signal / np.max(np.abs(signal)) * max_)
    else:
        raise ValueError('bit should be either 32 or 16')
    
    if remove_mean == True:
        signal = signal - signal.mean()
    
    
    write( filename, rate, signal)
    
    print(f'file {filename} saved as WAV')