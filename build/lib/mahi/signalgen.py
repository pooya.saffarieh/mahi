'''
A set of function which can produce different kind of signals.
This can be either noise or a deterministic signal.

Author: Pooya Saffarieh (p.saffarieh@nikhef.nl)
'''


# ------------------------------------------------------------------------------------------------------------

def generate_unif_noise(t,num_unifs):
    '''
    Make a noise based on Uniform distribution.
    '''
    
    noise = np.zeros(len(t))
    
    for n_ in range(num_unifs):
        
        noise = noise + np.random.uniform(-1,1,len(t))
    
    return noise

# ------------------------------------------------------------------------------------------------------------

def generate_sin_noise(t,num_sins):
    '''
    t: 1d array, time samples
    num_sins: int, number of sins to sum
    
    Randomly choose a phase and frequency and sum sin() together.
    Just changing amp and phase is not sufficient to create a noise (the result is still a single sin)
    We need to also randomly change freq.
    '''
    
    noise = np.zeros(len(t))
    
    for n_ in range(num_sins):
        
        phase = np.random.normal(0,np.pi)
        freq  = np.random.normal(0,1)
        noise = noise + np.sin(np.pi*2*freq*t + phase)
    
    return noise

# ------------------------------------------------------------------------------------------------------------

def henon(t, a=1.4, b=0.3, x0, x1):
    '''
    t: 1d array, time samples
    a: float, map parameter
    b: float, map parameter
    x0: float, initial value
    x1: float, initial value
    
    One dimensional Henon map.
    https://en.wikipedia.org/wiki/H%C3%A9non_map
    
    For (a,b)=(1.4,0.3) dynamic is chaotic (sensitive to intial value)
    '''
    
    x_n    = np.zeros(len(t))
    x_n[0] = x0
    x_n[1] = x1
    
    for n in range(1,len(t)-1):
        
            x_n[n+1] = 1 - a * x_n[n]**2 + b * x_n[n-1]
    
    return x_n

# ------------------------------------------------------------------------------------------------------------

def generate_henon_noise(t,num_henons):
    '''
    t: 1d array, holding the time samples.
    num_henons: number of henon maps to sum together to produce the final result.
    
    Sums num_henons henon map produced trajectories (with random initial value).
    '''
    
    noise = np.zeros(len(t))
    
    for n_ in range(num_henons):
        
        x0,x1 = np.random.uniform(-0.5,0.5),np.random.uniform(-0.5,0.5)
        noise = noise + henon( t, 1.4, 0.3, x0, x1)
    
    return noise

# ------------------------------------------------------------------------------------------------------------