'''

Functions which handle LTI systems or extend the functionalities of python-control package.

Author: P. Saffarieh (p.saffarieh@nikhef.nl)

'''

import control as ctr
import slycot
import scipy.fft as ssfft
from scipy.io import savemat


# -----------------------------------------------------------------------------------------------------------


def zpk2tf(z, p, k, Hz = False):
    '''
    Input:
    z: list, list of zeros in Rad (or Hz)
    p: list, list of poles in Rad (or Hz)
    k: float, constant gain before the fraction
    
    Return:
    Transfer function instance of python-control
    
    Description:    
    This function accepts ZeroPoleGain representation and returns transfer function object from control library.
    When Hz=True: the function accept pole/zero location in Hz and then transfer it to radian and negate it for stability.
    To do: It might be better to extend the control library class for doing this.
    '''
    z = np.array(z)
    p = np.array(p)
    if Hz == True:
        z = -1*z*(2*np.pi)
        p = -1*p*(2*np.pi)
    n, d = ss.zpk2tf(z,p,k)
    return ctr.TransferFunction(n,d)


# -----------------------------------------------------------------------------------------------------------


def cumrms(asd,f):
    '''
    Input:
    asd: 1d numpy array, 1d array of ASD
    f: 1d numpy array, 1d array of frequncies associated to ASD values
    
    Return:
    A tuple with frequencies as first value and cummulative sum of the RMS as second value
    It is tuple to make it easier for ploting. So you can just call plt.plot(*cumrms(*arg))
    
    Description: 
    
    A helper function that calculate cumulative RMS motion from right to left.
    The left most value shows the standard deviation or RMS of the time domain signal.
    The evolution of the curve from the right to left shows which frequencies mostly contribute to the increase of the 
    time series fluctuations.
    The output has the similar dimension as the input ASD. e.g m/sqrt(hz) >> m
    '''
    weighted_noise = (asd[1::]**2) * np.diff(f)  # m/sqrt(Hz)
    weighted_noise_inv = np.flip(weighted_noise)
    weighted_noise_inv_cum_sum = np.cumsum(weighted_noise_inv)
    weighted_noise_cum_sum = np.flip(weighted_noise_inv_cum_sum)
    weighted_noise_RMS_cum_sum = np.sqrt(weighted_noise_cum_sum)
    return (f[1:],weighted_noise_RMS_cum_sum)




# -----------------------------------------------------------------------------------------------------------

def roots(f, q):
    """
    Find the complex roots of poles/zeros given in frequency-q format

    :param f: list or numpy array of frequencies of the poles/zeros
    :param q: list or numpy array of the corresponding q factors
    :return: numpy array of complex poles/zeros
    """

    f = np.asarray(f, dtype='complex64')
    q = np.asarray(q, dtype='complex64')
    if not (np.all(q >= 0) and np.all(np.isfinite(q))):
        raise ValueError("pole or zero Qs must be finite and non-negative")

    if not np.all(q[f == 0] == 0):
        raise ValueError('Poles or zeros in DC must have Q = 0')

    so = (q != 0)
    mag = np.pi * f[so] / q[so]
    sq = np.sqrt(1 - 4 * q[so]**2)
    return np.concatenate((-2 * np.pi * f[~ so],
                           mag * (-1 + sq),
                           mag * (-1 - sq)))

# -----------------------------------------------------------------------------------------------------------

def virgo2zpk(zf, zq, pf, pq, g, gf):
    """
    Converts a zpq representation of a lti system to zpk
    :param zf: list or numpy array of frequencies of the zeros (in Hz)
    :param zq: list or numpy array of the corresponding q factors
    :param pf: list or numpy array of frequencies of the poles (in Hz)
    :param pq: list or numpy array of the corresponding q factors
    :param g: gain of the LTI system at frequency gf
    :param gf: frequency where g is set (in Hz)
    :return: zs, ps, k arrays of the complex zeros, poles and the total gain k
    """
    zs = roots(zf, zq)
    ps = roots(pf, pq)
    k = g / np.abs(ss.freqresp((zs, ps, 1), w=[gf * 2 * np.pi])[1])
    return zs, ps, k



# ------------------------------------------------------------------------------------------------------------


def Acond(sys,f):
    '''
    sys: python control state space
    f: frequency in Hz
    
    returns: scalar - condition number of A matrix at f
    
    Condition number of A matrix of a system at a given frequency in Hz
    '''
    return abs(np.linalg.cond((1j*f*2*np.pi)*np.identity(sys.A.shape[0])-sys.A))


# ------------------------------------------------------------------------------------------------------------


def round_complex_matrix(complex_matrix,n_decimal_digit):
    '''
    complex_matrix: a numpy 2d array with complex value as items.
    n_decimal_digit: int, number of decimals to use and discard others.
    
    Round a complex matrix. Treat real and imag part in the same way using round() function.
    '''
    return np.round(np.real(new_A_),n_decimal_digit) + 1j * np.round( np.imag(new_A_), n_decimal_digit)


# ------------------------------------------------------------------------------------------------------------


def syslice(sys,inname,outname):
    '''
    
    sys: python-control SS/IO model
    inname: a 1d array of input names to extract
    outname: a 1d array of output names to extract
    
    Slice a python-control SS system based on input output name instead of index number!
    
    '''
    
    i_in_  = sys.find_input(inname)
    i_out_ = sys.find_output(outname)
    
    if i_in_ is None or i_out_ is None:
        
        raise TypeError('Input or output name not found!')
    
    return sys[i_out_,i_in_]


# ------------------------------------------------------------------------------------------------------------

def stable_check(sys):
    '''
    sys: list of pycontrol sys or a sys
    
    Check stability by checking all the poles being in LHP.
    Should be improved by checking poles against a tolerance. 
    '''
    if isinstance(sys, list):
        return [np.alltrue(sys_.poles().real < 0) for sys_ in sys]
    else:     
        return np.alltrue(sys.poles().real < 0)

# ------------------------------------------------------------------------------------------------------------


def quad_sum(asd_list):
    '''
    asd_list: 2d array. first dimension sysN. Second dimension SySN freqresp vecotor.
    
    Return: the 1d array quad sum of ASDs.
    
    Example: quad_sum([sys1_freqresp,sys2_freqresp])
    
    Calculate quadrature sum of the ASDs.
    '''
    
    asd_summed = np.zeros(asd_list[0].shape[0])
    
    for asd_ in asd_list:
        
        asd_summed = np.sqrt(asd_summed**2 + asd_**2)
        
    return asd_summed


# ------------------------------------------------------------------------------------------------------------

def damp_fq(sys):
    '''
    sys: python-control system.
    
    return damping but with frequency in Hertz and Q factor.
    
    This is a wrapper for py-control .damp() but returns in Hz
    '''
    w_,d_,p_ = sys.damp()
    f_ = w_ / (2*np.pi)
    q_ = 1/(2*d_)
    print('freq. [Hz] / Q / Poles')
    return dict(zip(f_,q_)),f_,q_,p_


# ------------------------------------------------------------------------------------------------------------


def show_labels(sys):
    '''
    sys: list of pycontrol system
    
    A helper function that shows input/output names for many systems.
    '''
    if isinstance(sys, list):
        
        for s_ in sys:
            
            print(s_.name, 'Input names: ', s_.input_labels,"\n")
            print(s_.name, 'Output names: ', s_.output_labels,"\n")
    
    elif isinstance(sys, ctr.statesp.StateSpace):
        
        print(sys.name, 'Input names: ', sys.input_labels, "\n")
        print(sys.name, 'Output names: ', sys.output_labels, "\n")
        
    else:
        print(type(sys),' not supperted!')


# ------------------------------------------------------------------------------------------------------------


def ss_to_mat(sys,filename):
    '''
    sys: list of sys to export
    filename: filename to save
    
    Exporting everthing to .mat so we can work on the SS in matlab.
    '''

    sys_list_ = [generalized_plant]

    abcd_map = {}

    for s_ in sys_list_:

        abcd_map[s_.name+'_A'] = s_.A
        abcd_map[s_.name+'_B'] = s_.B
        abcd_map[s_.name+'_C'] = s_.C
        abcd_map[s_.name+'_D'] = s_.D
        abcd_map[s_.name+'_input_name'] = s_.input_labels
        abcd_map[s_.name+'_output_name'] = s_.output_labels

    savemat(f'{filename}.mat', abcd_map)
    
    
# ------------------------------------------------------------------------------------------------------------