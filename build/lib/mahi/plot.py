'''

Functions which make plotting easier.

Author: P. Saffarieh (p.saffarieh@nikhef.nl)

'''

import control as ctr
import slycot


# -----------------------------------------------------------------------------------------------------------

def mbode(sys,inputs,outputs,ylim,xticks):
    '''
    sys: python control system
    inputs: inputs to scan in order
    outputs: outputs to scan in order
    ylim: a list with two elements [start,end] - default: automatic
    xticks: indicate special xticks - default: [1e-4,1e-3,1e-2,1e-1,1,2,10,100]
    
    Bode for MIMO system. Extends the python-control Bode plot. Doesn't plot phase.
    '''
    system = RM_SS

    input_scan_list  = inputs
    output_scan_list = outputs

    n_outputs = len(output_scan_list)
    n_inputs  = len(output_scan_list)

    fig, axs = plt.subplots(n_outputs, n_inputs, sharex=True, layout='constrained')

    fig.set_size_inches(18.5, 10.5)

    for i in range(n_inputs):
        for o in range(n_outputs):

            input_number  = input_scan_list[i]
            output_number = output_scan_list[o]

            mag, _, _ = ctr.bode( system[output_number,input_number], omega=w, plot=False)
            axs[o,i].loglog(f, mag)
            axs[o,i].grid()
            if y_lim != None:
                axs[o,i].set_ylim(ylim)
            if x_lim == None:
                axs[o,i].set_xticks([1e-4,1e-3,1e-2,1e-1,1,2,10,100])
            else:
                axs[o,i].set_xticks(xticks)

            # Set the input names.
            if o == 0:
                axs[o,i].set_title(f'{system.input_labels[input_number]}',fontsize=20)
            # Set the output names.
            if i == 0:
                axs[o,i].set_ylabel(f'{system.output_labels[output_number]}        ',rotation=0,fontsize=20)
                
                
# -----------------------------------------------------------------------------------------------------------


def bode(sys,w=None,xticks=None,yticks=None,**args):
    '''
    sys: a pycontrol sys.
    w: frequencies in rad/s
    
    Shows the plot. Can't combine two bode yet.
    
    Simple helper function to plot bode diagrams.
    '''
    
    mag_,phase_,omega_ = ctr.bode(sys,w,Hz=True,plot=False,deg=True,**args)
    
    f_ = omega_ / (np.pi * 2)
    angle_ = 180 * (phase_ / np.pi)
    
    fig, (ax0, ax1) = plt.subplots(2, 1, layout='constrained')
    ax0.loglog(f_, mag_)
    ax0.set_xlabel('Freq. [Hz]')
    ax0.set_ylabel('mag.')
    ax0.grid(True)
    
    if yticks:
        ax0.set_yticks(yticks)
    if xticks:
        ax0.set_xticks(xticks)
        
    ax1.semilogx(f_,angle_)
    ax1.set_xlabel('Freq. [Hz]')
    ax1.set_ylabel('Phase [Deg.]')
    ax1.grid(True)

    plt.show()

# -----------------------------------------------------------------------------------------------------------


def invert_ss(sys, input_names, output_names):
    '''
    
    sys: py control SS
    
    Helper function to invert state space models.
    D matrix should be square, invertible matrix.

    '''
    
    # Extracting A, B, C, D matrix individually
    A, B, C, D = sys.A, sys.B, sys.C, sys.D

    # Inverting the state space. 
    # For derivation look here: https://engineering.stackexchange.com/questions/21895/how-do-i-invert-a-system-in-state-space-form

    Ai = (A - B @ inv(D) @ C)
    Bi = B @ inv(D)
    Ci = - inv(D) @ C
    Di = inv(D)

    # Now making it a state space.
    inv_sys = ctr.StateSpace(Ai, Bi, Ci, Di, inputs=input_names, outputs=output_names)
    
    return inv_sys
    


# -----------------------------------------------------------------------------------------------------------

 
def impulse(sys,t=None):
    '''
    sys: pycontrol system
    t: time samples in second. Default: automatic.
    
    Helper function to plot impulse reponse of a system.
    '''
    plt.plot(*ctr.impulse_response(sys,T=t))
    
    
# ------------------------------------------------------------------------------------------------------------