# setup script #
# I don't enforce requirements because they are typiclly already satisfied and some of the modules doesn't require some of the requirements.
# You need to manually increase the version number if you want the new built package to have a different version.

from setuptools import find_packages, setup

setup(
    name='mahi',
    packages=find_packages(),
    version='0.0.9',
    description='A set of tools that accelarate simulation and understanding of dynamical systems',
    author='Pooya Saffarieh',
    license='MIT',
    #install_requires=['control','scipy','matplotlib','numpy']
)
