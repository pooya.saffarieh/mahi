'''

Functions which make plotting easier.

Author: P. Saffarieh (p.saffarieh@nikhef.nl)

'''

import control as ctr
import slycot
import matplotlib.pyplot as plt 
import numpy as np

# ---------------------------------------------------------------------------------

def mbode(sys, w, inputs,outputs, ylim=None, xticks=None):
    '''
    sys: python control system
    inputs: inputs to scan in order
    outputs: outputs to scan in order
    ylim: a list with two elements [start,end] - default: automatic
    xticks: indicate special xticks - default: [1e-4,1e-3,1e-2,1e-1,1,2,10,100]
    
    Bode for MIMO system. Extends the python-control Bode plot. Doesn't plot phase.
    '''
    system = sys
    
    f = w*np.pi*2

    input_scan_list  = inputs
    output_scan_list = outputs

    n_outputs = len(output_scan_list)
    n_inputs  = len(output_scan_list)

    fig, axs = plt.subplots(n_outputs, n_inputs, sharex=True, layout='constrained')

    fig.set_size_inches(18.5, 10.5)

    for i in range(n_inputs):
        for o in range(n_outputs):

            input_number  = input_scan_list[i]
            output_number = output_scan_list[o]

            mag, _, _ = ctr.bode( system[output_number,input_number], omega=w, plot=False)
            axs[o,i].loglog(f, mag)
            axs[o,i].grid()
            if ylim != None:
                axs[o,i].set_ylim(ylim)
            if xticks != None:
                axs[o,i].set_xticks(xticks)

            # Set the input names.
            if o == 0:
                axs[o,i].set_title(f'{system.input_labels[input_number]}',fontsize=20)
            # Set the output names.
            if i == 0:
                axs[o,i].set_ylabel(f'{system.output_labels[output_number]}        ',rotation=0,fontsize=20)
                
                
# ----------------------------------------------------------------------------


def bode(sys,w=None,xticks=None,yticks=None,**args):
    '''
    sys: a pycontrol sys.
    w: frequencies in rad/s
    
    Shows the plot. Can't combine two bode yet.
    
    Simple helper function to plot bode diagrams.
    '''
    
    mag_,phase_,omega_ = ctr.bode(sys,w,Hz=True,plot=False,deg=True,**args)
    
    f_ = omega_ / (np.pi * 2)
    angle_ = 180 * (phase_ / np.pi)
    
    fig, (ax0, ax1) = plt.subplots(2, 1, layout='constrained')
    ax0.loglog(f_, mag_)
    ax0.set_xlabel('Freq. [Hz]')
    ax0.set_ylabel('mag.')
    ax0.grid(True)
    
    if yticks:
        ax0.set_yticks(yticks)
    if xticks:
        ax0.set_xticks(xticks)
        
    ax1.semilogx(f_,angle_)
    ax1.set_xlabel('Freq. [Hz]')
    ax1.set_ylabel('Phase [Deg.]')
    ax1.grid(True)

    plt.show()


# -------------------------------------------------------

 
def impulse(sys,t=None):
    '''
    sys: pycontrol system
    t: time samples in second. Default: automatic.
    
    Helper function to plot impulse reponse of a system.
    '''
    plt.plot(*ctr.impulse_response(sys,T=t))
    
    
# -------------------------------------------------------

def embedding_plot(signal,delay,fs,bins,grid=True,**args):
    
    '''
    signal: 1d array, time series to use for embedding plot
    delay: float, number of seconds to delay for first axis 
    fs: int, sampling frequency
    bins: number of histogram bins
    
    Plots a delay embedding plot for a given timeseries.
    More options:
    https://matplotlib.org/stable/gallery/statistics/hist.html#customizing-your-histogram
    '''

    shift = int(fs*delay)+1
    s_t   = signal[1:-shift]
    s_t_1 = signal[shift:-1]
    
    corrcoef_ = round(np.corrcoef(s_t_1,s_t)[0,1],3)

    fig, axs = plt.subplots( 1, 1, figsize=(7, 7))

    # We can increase the number of bins on each axis
    axs.hist2d(s_t_1, s_t, bins = bins,**args)
    axs.set_title(f'shift = {delay} [s] - num data points: {len(s_t)} - Corrolation: {corrcoef_}')
    axs.set_xlabel(f"Xt-{delay}[S]")
    axs.set_ylabel("Xt")
    if grid:
        plt.grid()
    plt.show()
    
    return s_t,s_t_1

# -------------------------------------------------------