'''

Functions which process time series.

Author: P. Saffarieh (p.saffarieh@nikhef.nl)

'''

import scipy.fft as ssfft
import numpy as np
import scipy.signal as ss
from scipy import linalg

# -----------------------------------------------------------------------------------------------------------



def autocoherence(signal,window_length,navg,fs):
    '''
    Input:
    signal: 1d numpy array, time domain signal 
    window_length: int, number of time samples in a window
    navg: int, number of averages for calculates coherence between each two consecuative window
    
    Return:
    f: 1d array, frequncies 
    Cxy: 1d array, coherence value of consecuative windows
    
    Description: 
    
    Breaks the signal into chunks with size nsegment and calculate coherence between consecuative chunks with nperseg
    parameter. Then returns an evolution of this auto coherence during the time.
    
    
    Example:
    
    window_number = np.arange(len(cac))
    plt.pcolormesh(window_number, fac, cac.transpose())
    plt.yscale('log')
    plt.colorbar(label="Coherence");
    plt.xlabel('Time [window number]')
    plt.ylabel('Frequency [Hz]')
    plt.title("Auto Coherence")
    
    To Do:
    1- handling median?
    
    '''
    nperseg = int(window_length/navg)                        # calculate how many points should be used for calculating FFT
    sig_len = len(signal)                                    # calcualte the length of the original signal
    n_windows = sig_len // window_length                     # calculate the number of windwos we have (time resolution)
    remove_n_points = sig_len % window_length                # points we need to get rid of because we should have integer number of window_length fit inside the signal.
    signal_small = signal[0:(sig_len - remove_n_points)]     # make the smaller signal
    signal_windowed = signal_small.reshape(-1,window_length) # make an array like: [[window1],[window2],...,[window_n]]
    
    print( f" Resolution in Hz {1/(nperseg/fs)}" )
    print( f" Window size in minutes {(window_length/fs)/60}")
    
    # create two signals which are shifted version of each other so we can calculate the coh for a lagged version of original signal.
    sig1 = signal_windowed[0:-2]   
    sig2 = signal_windowed[1:-1]
    
    # Cxy contains the coherence of first window with the second window. then second window with third window and so on...
    f, Cxy = ss.coherence(sig1, sig2, fs, nperseg=nperseg)
    
    
    return f, Cxy


# -----------------------------------------------------------------------------------------------------------

def sosfiltering(s,fs,z,p,k):
    
    '''
    Input:
    z,p,k: numpy array, zeros/poles/gain of the system.
    s: numpy array, time series to filter
    fs: float, sampling rate [Hz]
    
    Return:
    filtered signal
    
    Description:
    
    Doing direct bilinear transform and filtering will lead to numerical errors. (why? reference?)
    So we use second order sections here to eliminate that effect.
    
    Analog filter features might shift due to the bilinear transformation

    '''

    zd,pd,kd = ss.bilinear_zpk(z,p,k,fs)
    sos_sys  = ss.zpk2sos(zd,pd,kd)
    s_filtered = ss.sosfilt(sos_sys,s)
    
    return s_filtered


# -----------------------------------------------------------------------------------------------------------

def remove_transient(x, threshold = 10):

    '''
    Parameters:
    x: 1d-array, input signal assuming centered at zero
    threshold: float, constant factor for absolute median of the signal
    
    Return:
    x_cleaned: 1d-array, cleaned signal
    
    Remove the transient parts from a time series by calculating the absolute median and then zeroing
    values bigger in magnitude from this number.
    '''
    
    transient_threshold = np.median(abs(x))*threshold
    print("Threshold: ",transient_threshold)
    print("Points zeroed: ",np.sum(abs(x) > transient_threshold))
    x_cleaned = np.where(abs(x) > transient_threshold, 0, x)
    
    return x_cleaned   

# -----------------------------------------------------------------------------------------------------------

def entropy(x, win_size=2, median_split=False):
    
    '''
    Parameters:
    x: 1d array, signal to analyse. Should be a normalized signal from a Gaussian distribution.
    win_size: int, size of each window to compute the sequence for. The windows are overlapping.
    num_section: int, how many letters to divide signal to 
    
    Return:
    entropy: float
    
    Description:
    The input signal is assumed to be gaussian. At least it should be symetric. So the probablity of having x>0 and x<0 should be equal.
    Otherwise the symbols doesn't capture all the complexity of the signal.
    
    '''
    
    # symbolize the input signal. Just support two symbols.
    # The median split feature symbolize the signal in a way that we have equal number of symbolze in the final time series.
    if median_split == False:
        x_symbolized = np.where(x > 0, 1, 0)
    else:
        x_symbolized = np.where(x > np.median(x), 1, 0)
    
    #x_symbolized_chunked = []   
    # this code make over lapping segments of size win_size with step size 1.
    #for idx in range(x_symbolized.size):    
    #    if idx+win_size > x_symbolized.size:
    #        break
    #    x_symbolized_chunked.append(x_symbolized[idx:(idx+win_size)]) 
    #x_symbolized_chunked = np.array(x_symbolized_chunked)    
    
    x_symbolized_chunked = divide_array(x_symbolized, win_size, 1)

    _, count = np.unique(x_symbolized_chunked, axis=0, return_counts=True) 
    
    # it doesn't matter if we can find a sequence since in the formula that will be zero probability and rule out immediately.
    
    number_of_win = x_symbolized_chunked.shape[0]
    
    probabilities = count / number_of_win
   
    entropy = 0
    
    for p in probabilities:
        entropy += -(1/win_size) * p * np.log2(p)
        
    return entropy
        
# -----------------------------------------------------------------------------------------------------------

# made by chat gpt!
def divide_array(arr, section_size, overlap):
    """
    Divide a 1-dimensional array into equal overlapping sections.
    
    Parameters:
        arr (numpy.ndarray): The input array.
        section_size (int): The size of each section.
        overlap (int): The number of elements to overlap between adjacent sections.
    
    Returns:
        numpy.ndarray: A 2-dimensional array containing the sections.
    """
    if not (section_size > overlap):
        raise ValueError(f"section size can't be <= overlap size - {section_size} <= {overlap}")

    # Calculate the stride length and shape of the new array
    stride = arr.strides[0]
    shape = ((len(arr) - overlap) // (section_size - overlap), section_size)
    
    # Use stride_tricks to create a new array with overlapping sections
    sections = np.lib.stride_tricks.as_strided(arr, shape=shape, strides=(stride * (section_size - overlap), stride))
    
    return sections


# ------------------------------------------------------------------------------------------------------------

def whitening_filter(x,remove_transients=False,n_points_to_remove=None):
    
    '''
    
    Parameters:
    x: 1d-array, one dimensional input signal
    
    Returns:
    whitened_signal: 1d-array, one dimensional input signal
    
    Description:
    Whiten the input signal. So the spectrum of the signal doesn't look coloured. Doesn't keep the variance of the signal the same.
    Doing this operation will introduce a little bit transients at the start and the end of the signal.
    You can remove those with n_points_to_remove option. You can check it with a plot to see how big this number should be.
    
    Warning: This operation is acausal. Do not use it unless you know what are the consequences!
    
    '''
    
    x_fourier   = ssfft.rfft(x)
    whitened_x_fourier = x_fourier / abs(x_fourier)
    whitened_x = ssfft.irfft(whitened_x_fourier)

    if remove_transients == True:

        return whitened_x[n_points_to_remove:-n_points_to_remove]

    return whitened_x

# ------------------------------------------------------------------------------------------------------------


def ort( signal, win_length, fs, overlap="Half"):
    '''
    Calculate SVD of a time series based on a window length and sampling rate.
    
    signal:     1d np array
    win_length: window length in seconds
    fs:         sampling freq in Hz
    overlap; float, overlap in seconds
    '''
    sig_matrix = sig_2_matrix(signal,win_length, fs, overlap)

    return linalg.svd(sig_matrix)

# ------------------------------------------------------------------------------------------------------------

def project_2_u(u,sig_matrix):
    '''
    project a second signal with sig_matrix to components of first signal with SVD u.
    
    ## THIS SHOULD BE DIFFERENT. THERE SHOULD BE A NORMALIZATION!?? ##
    '''
    coef_list_ = []
    for u_num in range(u.shape[1]):
        coef_ = (sig_matrix.T @ u[:,u_num])@(sig_matrix.T @ u[:,u_num])
        coef_ = coef_ / sig_matrix.shape[1] # calculate mean!
        coef_list_.append(coef_)
    
    return np.array(coef_list_)

# ------------------------------------------------------------------------------------------------------------


def rec_s(u,s,v):
    """
    REcounstruct signal based on SVD matrices.
    """
    s_  = np.concatenate((np.diag(s),np.zeros((u.shape[1]-s.shape[0],s.shape[0]))))
    sig_matrix = u@s_@v
    signal = np.ravel(sig_matrix,order="F")
    
    return signal


# ---------------------------------------------------------------------------


def sig_2_matrix(signal,win_length, fs, overlap="Half"):
    '''
    signal: 1d array, original timeserie
    win_length: int, length of each window in seconds
    fs: int, sampling rate in Hz
    
    Divide a signal into smaller chunks and make a matrix.
    '''
    win_length_n = int(win_length * fs)

    if overlap == "Half":
        overlap_n = int(win_length_n/2)
    else:
        overlap_n = int(overlap * fs)

    sig_matrix = divide_array(signal, win_length_n, overlap_n).T
    

    #discard_last_n_points = int(len(signal)%(win_length*fs))
    #print(f"warning: {discard_last_n_points} removed from the end of signal")
    #if discard_last_n_points != 0:
    #    signal = signal[0:-discard_last_n_points]
    
    return sig_matrix #signal.reshape(int(win_length*fs),-1, order='F')

# ------------------------------------------------------------------------

def proj_filter(sig_matrix,u,n_first_basis):
    '''
    Parameters:
    ----
    sig_matrix: 2d array, sig_matrix of any given signal
    u: 2d array, matrix holding the basis vectors to project on. 
    n_first_basis: int, number of coloumns in u to use for filtering.
    
    Description:
    ----
    Project a signal (as sig_matrix) into n_first_basis of u matrix.
    Each coloumn in u is representing a basis vector. The basis vectors in u are ordered based on singular values.
    n_first_basis parameter chooses only n most dominant basis vectors.
    '''
    
    
    basis_matrix = u[:,0:n_first_basis] # coloumns are basis
    
    dot_product = signal_matrix.T @ basis_matrix
    
    project_product_matrix = basis_matrix @ dot_product.T
    
    return project_product_matrix

# ------------------------------------------------------------------------------------------------------------

def matrix_2_sig(sig_matrix,overlap=None):
    '''
    sig_matrix: 2d numpy array
    
    Convert a sig_matrix to signal again. 
    Coloumns in a sig_matrix are consecuative windows in the signal. No overlaping supported.
    '''
    if overlap != None:
        ValueError('Overlap not supported. ')
    
    signal = np.ravel(sig_matrix,order="F")
    
    return signal

# ------------------------------------------------------------------------------------------------------------